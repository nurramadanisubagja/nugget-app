import { Button, Form, FormGroup, Label, Input, FormText ,Modal, ModalHeader, ModalBody,} from 'reactstrap';
import React, { Component } from 'react'
import Axios from 'axios'
import Url from '../../Component/UrlSource'

export class FormProject extends Component {

    constructor(props) {
        super(props)
        this.state = {
            id : this.props.id,
            project: '',
            desc: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleFile = this.handleFile.bind(this)
    }

    

    getDetailProject = () => {
        if(!this.state.id) {
            return false
        } else {

            Axios.get(Url.UrlApi + 'api/ProjectAll?id=' + this.state.id)
            .then(res => {
                this.setState({
                    data: res.data.data[0],
                    project: res.data.data[0]['project_name'],
                    desc: res.data.data[0]['description']
                })
                console.log(this.state.data)
            }).catch(err => {
                console.log('err' + err)
            })
        }
    }

    handleChange(event) {
        const name = event.target.name;
        const value = event.target.value;
    
        this.setState({
          [name]: value
        })
      }
    handleFile(event) {
        const name = event.target.name;
        const value = event.target.files[0];
    
        this.setState({
          [name]: value
        })

        console.log(value)

      }

    Submit(e) {
        e.preventDefault()
        if(this.state.id === '') {
            // insert data
        }
        else {
            // update data
        }

    }

    componentDidMount(){
        this.getDetailProject()
    }

    render() {
        return (
            <div>
            <Modal isOpen={this.props.open} toggle={this.props.toggle} className={this.props.className}>
            <ModalHeader toggle={this.props.toggle}>{this.props.title}</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup style={{ padding: '0px 15px' }}>
                            <Label for="exampleProject" className='font-weight-bold' >Project Name :</Label>
                            <Input type="text" name="project" id="exampleProject" onChange={this.handleChange} value={ this.state.project } required placeholder="with a Project" />
                        </FormGroup>
                            <hr/>
                        <FormGroup style={{ padding: '0px 15px' }}>
                            <img src="" className="rounded" alt="Cinque Terre" width="100" height="100" />
                        </FormGroup>
                        <FormGroup style={{ padding: '0px 15px' }}>
                            <Label for="exampleProject" className='font-weight-bold'>Icon :</Label>
                            <Input type="file" name="file" id="exampleProject" onChange={this.handleFile} required placeholder="with a Project" />
                        </FormGroup>
                        <hr />
                        <FormGroup style={{ padding: '0px 15px' }}>
                            <Label for="exampleProject" className='font-weight-bold'>Description</Label>
                            <textarea className="form-control" name='desc' required id="exampleFormControlTextarea1" onChange={this.handleChange} value={ this.state.desc} rows="4" ></textarea>
                        </FormGroup>
                        <div className='d-flex justify-content-center'>
                                <Button className='mr-3' onClick={this.props.onClick} color="danger">Cancel</Button>
                                <Button  type="submit" onClick={this.props.onSubmit} color="success">Simpan</Button>
                        </div>
                    </Form> 
                </ModalBody>
            </Modal>
        </div>
        )
    }
}


export default FormProject;