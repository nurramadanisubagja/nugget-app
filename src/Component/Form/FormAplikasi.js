import React, { Component } from 'react'
import { Button, Form, FormGroup, Label, Input, FormText ,Modal, CustomInput, ModalBody,} from 'reactstrap';
import {Link} from 'react-router-dom';
import Axios from 'axios'

class FormAplikasi extends Component {
    constructor(props){
        super(props)

        this.state = {
            step: 1,
            app_name: '',
            dbms: '',
            ip_address: '',
            port: '',
            username: '',
            password: '',
            db_name: '',
            pass_db: '',
            docker: '',
            connection: '',
            user_repo: '',
            pass_repo: '',
            error : '',
            id: this.props.id
        }
    }

    nextStep = () => {
        const { step, app_name } = this.state
        if(app_name === '') {
        this.setState({
            step: step
            })
        } else {
            this.setState({
                step: step + 1
            })
        }
        console.log(this.state.id_project)
    }
    
    prevStep = () => {
        const { step } = this.state
        this.setState({
            step: step - 1
        })
    }

    getAplicationDetail () {
        if(this.state.id === '}') {
            console.log('Ini Tambah Data')
        } else {
            console.log("ini Edit")
        }
    }

    componentDidMount() {
        this.getAplicationDetail()
    }

    handleChange = input => e => {
        this.setState({
            [input] : e.target.value
        })
    }

    render() {
        const { step } = this.state
        const { app_name, dbms, ip_address, port ,username, password, db_name, pass_db, docker, connection, user_repo, pass_repo } = this.state
        const { values } = { app_name, dbms, ip_address, port ,username, password, db_name, pass_db, docker, connection, user_repo, pass_repo }
        switch(step) {
            case 1 :
                
                 return (
                        <Step_satu 
                        nextStep={this.nextStep}
                        handleChange={ this.handleChange }
                        values={ values }
                        id={this.state.id}
                        />
                    )
            
            case 2: 
            return(
                <Step_dua 
                nextStep={this.nextStep}
                prevStep={this.prevStep}
                onChange={this.handleChange}
                values={values}
                />
            )
            case 3:
            return(
                <Step_tiga 
                nextStep={this.nextStep}
                prevStep={this.prevStep}
                />
            )
        }

    }
}

class Step_satu extends Component {

    state = {
        id : this.props.id,
        id_project : localStorage.getItem('id_project')
    }

    berikutnya = e => {
        e.preventDefault()
            this.props.nextStep()
    }

    render() {
        const { values, handleChange } = this.props
        return(
            <div className='container-fluid' style={{border: '1px solid transparent'}}>
                <div className="step">
                    <div className="float-left b3 p5"> 
                        Step 1
                    </div>
                    <div className="float-left p5">
                        Step 2
                    </div>
                    <div className="float-left p5" >
                        Step 3
                    </div>
                </div>
                <div className="wrapper1">
                        <Form>
                            <div className="row pt-3">
                                <div className="col-md-12">
                                    <b>CREATE NEW APPLICATION</b>
                                    <hr />
                                </div>
                                <div className="col-md-6">
                                    <FormGroup >
                                        <Label for="exampleProject">Appication Name</Label>
                                        <Input type="text" name="app_name" id="exampleProject" placeholder="Oranger web" onChange={handleChange('app_name')}/>
                                    </FormGroup>
                                </div>
                                <div className="col-md-6">
                                    <Label for="exampleSelect">DBMS</Label>
                                    <Input type="select" name="dbms" id="exampleSelect">
                                        <option>Oracle 12c</option>
                                        <option>mysql</option>
                                        <option>postgree</option>
                                    </Input>
                                </div>
                            </div>
                            <div className="row" >
                                <div className="col-md-6">
                                    <FormGroup>
                                        <Label for="exampleProject">IP Address</Label>
                                        <Input type="text" name="ip_address" id="exampleProject" required placeholder="3.0.84.43" />
                                    </FormGroup>
                                </div>
                                <div className="col-md-6">
                                    <FormGroup>
                                        <Label for="exampleProject">Port</Label>
                                        <Input type="text" name="port" id="exampleProject" required placeholder="8080" />
                                    </FormGroup>
                                </div>
                            </div>
                            <div className="row" >
                                <div className="col-md-6">
                                    <FormGroup>
                                        <Label for="exampleProject">Username</Label>
                                        <Input type="text" name="username" id="exampleProject" required placeholder="Rudi Istanbul" />
                                    </FormGroup>
                                </div>
                                <div className="col-md-6">
                                    <FormGroup>
                                        <Label for="exampleProject">Password</Label>
                                        <Input type="password" name="password" id="exampleProject" required placeholder="Password" />
                                    </FormGroup>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                    <FormGroup>
                                        <Label for="exampleProject">Database Name</Label>
                                        <Input type="text" name="db_name" id="exampleProject" required placeholder="Database" />
                                    </FormGroup>
                                </div>
                                <div className="col-md-6">
                                    <FormGroup>
                                        <Label for="exampleProject"> Password</Label>
                                        <Input type="password" name="pass_db" id="exampleProject" required placeholder="Password Database" />
                                    </FormGroup>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                <FormGroup>
                                        <CustomInput type="checkbox" id="exampleCustomCheckbox" label="Docker" />
                                    </FormGroup>
                                </div>
                            </div>
                            <div className="row pt-3">
                                <div className="col-md-12">
                                    <b>Repository</b>
                                    <hr/>
                                </div>
                                <div className="col-md-6">
                                    <FormGroup >
                                        <Label for="exampleProject">Connection</Label>
                                        <Input type="text" name="connection" id="exampleProject" required placeholder="Connection" />
                                    </FormGroup>
                                </div>
                                <div className="col-md-6">
                                    <FormGroup >
                                        <Label for="exampleProject">Username</Label>
                                        <Input type="text" name="user_repo" id="exampleProject" required placeholder="Username" />
                                    </FormGroup>
                                </div>
                                <div className="col-md-6">
                                    <FormGroup >
                                        <Label for="exampleProject">Password</Label>
                                        <Input type="text" name="pass_repo" id="exampleProject" required placeholder="Password" />
                                    </FormGroup>
                                </div>
                            </div>
                            <div className='d-flex justify-content-center' style={{padding: '15px 0px'}}>
                            <Link to={`/nugget-web/Detail-Project/${this.state.id_project}`} className='link'>
                                <Button className='mr-3' type="button" color="danger">Kembali</Button>
                             </Link>   
                                <Button className='mr-3' onClick={this.berikutnya} type="button" color="success">Berikutnya</Button>
                            </div>
                        </Form>
                </div>
            </div>
        )
    }
}

class Step_dua extends Component {

    berikutnya = e => {
        e.preventDefault()
        this.props.nextStep()
    }
    kembali = e => {
        e.preventDefault()
        this.props.prevStep()
    }

    render() {
        return(
            <div className='container-fluid' style={{border: '1px solid transparent'}}>
                <div className="step">
                    <div className="float-left p5"> 
                        Step 1
                    </div>
                    <div className="float-left b3 p5">
                        Step 2
                    </div>
                    <div className="float-left p5" >
                        Step 3
                    </div>
                </div>
                <div className="wrapper1">
                        <Form>
                            <div className="row pt-3">
                                <div className="col-md-12">
                                    <b>Secutiry</b>
                                    <hr/>
                                </div>
                                <div className="col-md-6">
                                    <FormGroup >
                                        <Label for="exampleProject">Connection</Label>
                                        <Input type="text" name="connection" id="exampleProject" required placeholder="Oranger web" />
                                    </FormGroup>
                                </div>
                                <div className="col-md-6">
                                    <FormGroup >
                                        <Label for="exampleProject">Username</Label>
                                        <Input type="text" name="user_repo" id="exampleProject" required placeholder="Oranger web" />
                                    </FormGroup>
                                </div>
                                <div className="col-md-6">
                                    <FormGroup >
                                        <Label for="exampleProject">Password</Label>
                                        <Input type="text" name="pass_repo" id="exampleProject" required placeholder="Oranger web" />
                                    </FormGroup>
                                </div>
                            </div>
                            <div className='d-flex justify-content-center' style={{padding: '15px 0px'}}>
                                <Button className='mr-3'  onClick={this.kembali} type="button" color="danger">Kembali</Button>
                                <Button className='mr-3' onClick={this.berikutnya} type="button" color="success">Berikutnya</Button>
                            </div>
                        </Form>
                </div>
            </div>
        )
    }
}

class Step_tiga extends Component {

    berikutnya = e => {
        e.preventDefault()
        this.props.nextStep()
    }
    kembali = e => {
        e.preventDefault()
        this.props.prevStep()
    }
    render() {
        return(
            <div className='container-fluid' style={{border: '1px solid transparent'}}>
                <div className="step">
                    <div className="float-left p5"> 
                        Step 1
                    </div>
                    <div className="float-left p5">
                        Step 2
                    </div>
                    <div className="float-left b3 p5">
                        Step 3
                    </div>
                </div>
                <div className="wrapper1">
                        <Form>
                            <div className="row pt-3">
                                <div className="col-md-12">
                                    <b>Secutiry</b>
                                    <hr/>
                                </div>
                                <div className="col-md-6">
                                    <FormGroup >
                                        <Label for="exampleProject">Connection</Label>
                                        <Input type="text" name="connection" id="exampleProject" required placeholder="Oranger web" />
                                    </FormGroup>
                                </div>
                                <div className="col-md-6">
                                    <FormGroup >
                                        <Label for="exampleProject">Username</Label>
                                        <Input type="text" name="user_repo" id="exampleProject" required placeholder="Oranger web" />
                                    </FormGroup>
                                </div>
                                <div className="col-md-6">
                                    <FormGroup >
                                        <Label for="exampleProject">Password</Label>
                                        <Input type="text" name="pass_repo" id="exampleProject" required placeholder="Oranger web" />
                                    </FormGroup>
                                </div>
                            </div>
                            <div className='d-flex justify-content-center' style={{padding: '15px 0px'}}>
                                <Button className='mr-3'  onClick={this.kembali} type="button" color="danger">Kembali</Button>
                                <Button className='mr-3' onClick={this.berikutnya} type="button" color="success">Berikutnya</Button>
                            </div>
                        </Form>
                </div>
            </div>
        )
    }
}

class Step_empat extends Component {
    render() {
        return(
            <h1>Form 4</h1>
        )
    }
}

export default FormAplikasi
