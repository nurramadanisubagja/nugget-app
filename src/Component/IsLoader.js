import React, { Component } from 'react'
import Loader from 'react-loader-spinner'
export class IsLoader extends Component {
    render() {
        return (
            <div className="loader">
                <Loader
                    type="Plane"
                    color="#00BFFF"
                    height="50"
                    width="50"
                    className="transform"
                />
            </div>
        )
    }
}

export default IsLoader
