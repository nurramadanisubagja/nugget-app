import React from 'react';
import logo from './logo.svg';
import './App.css';
import CreateProject from '../src/view/Project/CreateProject'
import CreateAplication from './view/Project/CreateAplication';
import NewAplication from './view/Project/NewAplication';
import flowchart from './Component/flowchart/flowchart'
import Login from './view/Login/Login'
import CreateFlowChart from './view/Project/CreateFlowChart'
import Page404 from './view/Page404';
import { BrowserRouter as Router,Route, Link, Switch } from "react-router-dom";

function App() {
  return (
    <div>
      <Router>
        <Switch>
          <Route exact path='/' component={Login} />
          <Route path='/nugget-web/create-Project' component={CreateProject}/>
          <Route path='/nugget-web/Detail-Project/:id' component={CreateAplication}/>
          <Route path='/nugget-web/Create-Aplication/:id' component={NewAplication}/>
          <Route path='/nugget-web/FlowChart/:id' component={CreateFlowChart}/>
          <Route path='/not-found-404' component={Page404}/>
          <Route path='/nugget-web/Create-FlowChart/:id' component={flowchart}/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
