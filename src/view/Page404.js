import React, { Component } from 'react'
import logo from '../assets/Image/page-not-found.png'
export class Page404 extends Component {
    render() {
        return (
            <div>
                <img src={logo} height='400' style={{position: 'absolute', margin: '150px 400px'}}/>
            </div>
        )
    }
}

export default Page404
