import React, { Component } from 'react'
import '../../scss/style.css'
import {Link} from 'react-router-dom'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown, faHome, faChartPie, faQuestionCircle, faCogs, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import logo from '../../logo.svg';

export class Header extends Component {


    render() {
        return (
            <div>
                
                <div className='Header'>
                     <h2 style={{color: 'white'}}>Ngt Admin</h2>
                </div>
                <div className="user-nav">
                <img src={ logo } className="rounded-circle" alt="Cinque Terre" width="50" height="50" /> <p className="dropdown">Ramadani@mail.com </p><FontAwesomeIcon icon={ faAngleDown } style={{margin: '0 5px'}}/>
                    <div className="dropdown-content">
                     <Link to={`/`}> <FontAwesomeIcon icon={ faSignOutAlt } /> Logout</Link>
                    </div>
                </div>
                <div className='sidebar'>
                    <ul className='menu-sidebar'>
                        <li><FontAwesomeIcon icon={ faCogs } style={{margin: '0 5px'}} /><span><Link to={`/nugget-web/create-project`} className='link-header' color="white"> Project</Link></span></li>
                        <li><FontAwesomeIcon icon={ faChartPie } style={{margin: '0 5px'}} /><span>BAM</span></li>
                        <li><FontAwesomeIcon icon={ faQuestionCircle } style={{margin: '0 5px'}} /><span>Help</span></li>
                    </ul>
                </div>
                <div className='content'>
                    { this.props.children }
                </div>
            </div>
        )
    }
}

export default Header
