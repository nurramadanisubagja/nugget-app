import React, { Component } from 'react'
import Header from '../Header/Header'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBan, faInfoCircle, faCogs, faEdit, faChartPie } from '@fortawesome/free-solid-svg-icons'
import { Button } from 'reactstrap'
import {Link} from 'react-router-dom';

export class CreateFlowChart extends Component {

    constructor(props){
        super(props)

        this.state = {
            id_projects : localStorage.getItem('id_project')
        }

    }

    render() {
        return (
            <div>
                <Header>
                <div className='content-header'>
                    <p  style={{padding: '5px 20px'}}><FontAwesomeIcon icon={ faCogs } /><Link to={`/nugget-web/create-Project`} className='link'> Project</Link> > 
                    <Link to={`/nugget-web/Detail-Project/${this.state.id_projects}`} className='link'> Aplication > </Link>
                    Create New FlowChart</p>
                </div>
                <div className='container-fluid' style={{border: '1px solid transparent'}}>
                <Link to={`/nugget-web/create-FlowChart/}`} className='link'>
                        <Button className='btn-bn mt-4' color="success">+ Create FlowChart</Button>
                    </Link>
                <div className="wraper">
                    aaaaaaaa
                </div>
            </div>
                </Header>
            </div>
        )
    }
}

export default CreateFlowChart
