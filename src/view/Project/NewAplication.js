 import React, { Component } from 'react'
import Header from '../Header/Header'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown, faHome, faChartPie, faQuestionCircle, faCogs, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import {Link} from 'react-router-dom'
import FormAplikasi from '../../Component/Form/FormAplikasi'

export class NewAplication extends Component {
    constructor(props){
        super(props)

        this.state = {
            id : this.props.match.params.id,
            id_projects : localStorage.getItem('id_project')
        }
    }
    render() {

        return (
            <div>
                <Header>
                <div className='content-header'>
                    <p  style={{padding: '5px 20px'}}><FontAwesomeIcon icon={ faCogs } /><Link to={`/nugget-web/create-Project`} className='link'> Project</Link> > 
                    <Link to={`/nugget-web/Detail-Project/${this.state.id_projects}`} className='link'> Aplication > </Link>
                    Create New Aplication</p>
                </div>
                <FormAplikasi 
                id={this.state.id}
                />
                </Header>
            </div>
        )
    }
}

export default NewAplication
