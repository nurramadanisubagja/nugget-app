import React, { Component } from 'react'
import Header from '../Header/Header'
import FormProject from '../../Component/Form/FormProject'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBan, faInfoCircle, faCogs, faEdit, faChartPie } from '@fortawesome/free-solid-svg-icons'
import {BootstrapTable, TableHeaderColumn,  SearchField} from 'react-bootstrap-table';
import Axios from 'axios'
import Url from '../../Component/UrlSource'
import { Button } from 'reactstrap'
import {Link} from 'react-router-dom';
import IsLoader from '../../Component/IsLoader'

export class CreateAplication extends Component {

    constructor(props){
        super(props)

        this.state = {
            isOpen : false,
            isLoader : true,
            data : [],
            app : [],
            id : this.props.match.params.id

        }

        this.toggle = this.toggle.bind(this)
    }

    createCustomSearchField = (props) => {
        return (
        
          <SearchField
              className='my-custom-class'
              defaultValue=''
              placeholder='Search'/>
        );
      }


    getDetailProject = () => {
        Axios.get(Url.UrlApi + 'api/ProjectAll?id=' + this.state.id)
        .then(res => {
            this.setState({
                data: res.data.data[0],
                isLoader: false
            })
            console.log(this.state.data)
        }).catch(err => {
            console.log('err' + err)
        })
    }

    getAplication = () => {
        Axios.get(Url.UrlApi + 'api/getDetailAplikasi?project_id=' + this.state.id)
        .then(res => {
            this.setState({
                app : res.data.data
            })
        }).catch(err => {
            console.log(err)
        })
    }


    actionTable = (cell, row) => {
        
        if( this.state.app[0] === 'Data Belum Ada' ) {
            return 
        } else {
            
            return (
                <div className="d-flex justify-content-center">
                <Link to={`/nugget-web/Create-Aplication/${row['id']}`} className='link'>
                  <button
                    className="btn btn-info btn-sm"
                    style={{ marginRight: 3 }}
                  >
                      {localStorage.setItem('id_apps', row['id'])}
                    <FontAwesomeIcon icon={ faInfoCircle } />
                  </button>
                  </Link>
                  <Link to={`/nugget-web/FlowChart/${row['id']}`} className='link'>
                  <button
                    className="btn btn-primary btn-sm"
                    style={{ marginRight: 3 }}
                  >
                    <FontAwesomeIcon icon={ faChartPie } />
                  </button>
                  </Link>
                  <Link to={`/nugget-web/Create-Aplication/${row['id']}`} className='link'>
                  <button
                    className="btn btn-success btn-sm"
                    style={{ marginRight: 3 }}
                  >
                    <FontAwesomeIcon icon={ faEdit } />
                  </button>
                  </Link>
                  <button
                    className="btn btn-danger btn-sm"
                    style={{ marginRight: 3 }}
                  >
                    <FontAwesomeIcon icon={ faBan } />
                  </button>
                  
                </div>
            )}
    }

    indexN = (cell, row, enumObject, index) => {
        if( this.state.app[0] === 'Data Belum Ada' ) {
            return (<div></div>)
        } else {
            return (<div>{index+1}</div>) 
        }
    }

    
    toggle = () => {
        this.setState({
            isOpen : !this.state.isOpen
        })
    }
    
    componentDidMount() {
        this.getAplication();
        this.getDetailProject();
    }

    render() {
        const options = {
            page: 1,  // which page you want to show as default\
            sizePerPageList: 
            [ 
                {
              text: '5', 
              value: 5
            }
            , 
            {
              text: '10', 
              value: 10
            } 
        ], // you can change the dropdown list for size per page
            sizePerPage: 5,  // which size per page you want to locate as default
            pageStartIndex: 1, // where to start counting the pages
            paginationSize: 3,  // the pagination bar size.
            prePage: 'Prev', // Previous page button text
            nextPage: 'Next', // Next page button text
            firstPage: 'First', // First page button text
            lastPage: 'Last', // Last page button text
            paginationShowsTotal: this.renderShowsTotal,  // Accept bool or function
            paginationPosition: 'bottom' , // default is bottom, top and both is all available
            searchField: this.createCustomSearchField,
            noDataText: 'Data Tidak Ada',
            // hideSizePerPage: true // You can hide the dropdown for sizePerPage
            // alwaysShowAllBtns: true // Always show next and previous button
            // withFirstAndLast: false > Hide the going to First and Last page button
          };

          let load;
          let { id } = this.state;
          localStorage.setItem('id_project', id);
          if(this.state.isLoader) {
              load = <IsLoader />
          }
          return (
              
              <div>
                  {load}
                <Header>
                <div className='content-header'>
                    <p  style={{padding: '5px 20px'}}><FontAwesomeIcon icon={ faCogs } /><Link to={`/nugget-web/create-project`} className='link'> Project</Link> > 
                    Aplication </p>
                </div>
                <div className='container-fluid' style={{border: '1px solid transparent'}}>
                    <div className='wraper'>
                        <div className='row' style={{padding : '10px 40px'}}>
                                 <div className='col-xs-6'>
                                     <img src={Url.UrlImage + 'Project/' + this.state.data.icon} className="img-rounded"  style={{border : '1px solid rgba(0,0,0,0.1)',boxShadow: '0px 1px 5px 0px rgba(0,0,0,0.2)'}} alt="Cinque Terre" width="130" height="150" /> 
                                 </div>
                                 <div className='col-xs-6 pl-2'>
                                    <span className='font-weight-bold'>Project Name : </span>
                                    <p>{ this.state.data.project_name }</p>
                                    <span className='font-weight-bold'>Description : </span>
                                    <p>
                                    { 
                                        this.state.data.description
                                    }
                                    </p>
                                </div>
                                <hr />
                                <span className='text' onClick={this.toggle}>Edit</span>
                         </div>
                    </div>
                    <Link to={`/nugget-web/Create-Aplication/}`} className='link'>
                        <Button className='btn-bn' color="success" data-toggle="tooltip" data-placement="bottom" title="Create Aplication FrontEnd  ">+ Create Aplication</Button>
                    </Link>
                    <div className='wraper' style={{padding: '15px 15px'}}>
                        <div>
                            <BootstrapTable 
                            data={ this.state.app } 
                            pagination={ true } 
                            options={ options } 
                            search striped hover>
                                <TableHeaderColumn dataField='any' dataFormat={this.indexN} >No</TableHeaderColumn>
                                <TableHeaderColumn dataField='app_name' isKey>App name</TableHeaderColumn>
                                <TableHeaderColumn dataField='dbms'>DBMS</TableHeaderColumn>
                                <TableHeaderColumn dataField='database_name'>Database Name</TableHeaderColumn>
                                <TableHeaderColumn dataField='connect' >Connect</TableHeaderColumn>
                                <TableHeaderColumn dataField='backend'>Backend</TableHeaderColumn>
                                <TableHeaderColumn className="text-center" dataFormat={this.actionTable}>Aksi</TableHeaderColumn>
                            </BootstrapTable>
                         </div>
                    </div>
                    <FormProject 
                       open={this.state.isOpen}
                       onClick={this.toggle}
                       toggle={this.toggle}
                       className={this.props.className}
                       id={this.state.id}
                       title="Edit Project"
                       />
                </div>
                </Header>
            </div>
        )
    }
}

export default CreateAplication
