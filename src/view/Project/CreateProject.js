import React, { Component } from 'react'
import Header from '../Header/Header'
import FormProject from '../../Component/Form/FormProject'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown, faHome, faChartPie, faQuestionCircle, faCogs, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import axios from 'axios'
import Loader from 'react-loader-spinner'
import Url from '../../Component/UrlSource'
import IsLoader from '../../Component/IsLoader'
import {Link} from 'react-router-dom';


export class CreateProject extends Component {

    
    constructor(props){
        super(props)

        this.state = {
            isOpen : false,
            isLoader: true,
            kata : [],
            data : []
        }

        this.toggle = this.toggle.bind(this)
    }


    getProject = () => {
        axios.get(Url.UrlApi + 'api/getAllProject')
        .then(res => {
            this.setState({
                data : res.data.data,
                isLoader: false
            })
            console.log(this.state.data)
            console.log(this.state.data.length)
        }).catch(err => {
            console.log('error euy ' + err)
        })
    }

 

    deleteContent = () => {

        window.confirm('yakin , mau di hapus nih ?? ');

    }

    toggle = () => {
     
        this.setState({ 
            isOpen : !this.state.isOpen
        })
    }

    componentDidMount() {
        this.getProject()
    }
    



    render() {
        let load;
        if(this.state.isLoader) {
            load = <IsLoader />
        }
        return (
            <div>
                {load}
                <Header>
                <div className='content-header'>
                    <p  style={{padding: '5px 20px'}}><FontAwesomeIcon icon={ faCogs } /> Project</p>
                </div>
                    <div className='container-fluid'>
                       <Button className='btn-custom' onClick={this.toggle} color="success">+ Create Project</Button>
                       <div className='wrapper' id='style-2'>
                                <div className='row' style={{padding: '15px 15px'}}>
                                    {
                                        this.state.data.map((item, key) => {
                                            let desc;
                                            if(item.description.length <= 20) {
                                                desc = item.description
                                            } else {
                                                desc = item.description.substr(0,20) + ' . . .'
                                            }
                                            return(
                                                <div key={ key } className='col-lg-3  content-wraper' style={{}}>
                                                <span style={{position : 'absolute', right: '0', marginRight: '5px'}} onClick={this.deleteContent}> X </span>
                                                <Link to={`/nugget-web/Detail-Project/${item.id}`} className='link'>
                                                <div className='row' style={{padding : '10px 10px'}}>
                                                    <div className='col-xs-6'>
                                                        <img src={Url.UrlImage + 'Project/' + item.icon} className="img-rounded"  style={{border : '1px solid rgba(0,0,0,0.1)',boxShadow: '0px 1px 5px 0px rgba(0,0,0,0.2)'}} alt="Cinque Terre" width="100" height="120" /> 
                                                    </div>
                                                    <div className='col-xs-6 pl-2'>
                                                            <span className='font-weight-bold'>Project Name : </span>
                                                            <p>{ item.project_name }</p>
                                                            <span className='font-weight-bold'>Description : </span>
                                                            <p>{ desc }</p>
                                                    </div>
                                                </div>
                                                </Link>
                                            </div>
                                            )
                                        })
                                    }
                                </div>
                       </div>
                       <FormProject 
                       open={this.state.isOpen}
                       onClick={this.toggle}
                       toggle={this.toggle}
                       className={this.props.className}
                       title="Tambah Project"
                       />
                    </div>
                </Header>
            </div>
        )
    }
}

export default CreateProject
